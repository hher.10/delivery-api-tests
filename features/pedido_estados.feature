# language: es

Característica: Creacion y Cambios de estado de Pedidos

  """
  Se ofrecen 3 menus:
    Menu individual => $100)
    Menu parejas ====> $175
    Menu familiar ===> $250
  
  Para hacer pedidos el cliente debe previamente haber completado su registacion con domicilio y telefono
  """

  Antecedentes:
    Dado el cliente "jperez"
    Y se registra con domicilio "Cucha Cucha 1234 1 Piso B" y telefono "4123-4123"
    Y que el repartidor "juanmotoneta" esta registrado

  Escenario: P1 - Hace pedido exitoso
    Cuando el cliente pide un "menu_individual"
    Entonces obtiene numero de pedido único

  Escenario: P2 - menu invalido
    Cuando el cliente pide un "menu_ejecutivo"
    Entonces obtiene error por pedido inválido

  Escenario: CP1 - Consulta de pedido
    Dado que el cliente pidio un "menu_individual"
    Cuando consulta el estado
    Entonces el estado es "recibido"

  Escenario: CP2 - Consulta de pedido inexistente
    Dado que el cliente no hizo pedidos
    Cuando consulta el estado de un pedido
    Entonces obtiene un mensaje indicando que no realizo pedidos

  Escenario: CP3 - Consulta de pedido de otro cliente
    Dado que el cliente pidio un "menu_individual"
    Cuando consulta el estado de un pedido que no hizo el
    Entonces obtiene un mensaje de error indicando que la orden no existe

  Escenario: CEP1 - cambio de estado de recibida a en_preparacion
    Dado que el cliente pidio un "menu_pareja"
    Cuando el estado cambia a "en_preparacion"
    Y consulta el estado
    Entonces el estado es "en_preparacion"

  Escenario: CEP2 - cambio de estado de en_preparacion a en_entrega
      Dado que el cliente pidio un "menu_familiar"
      Cuando el estado cambia a "en_entrega"
      Y consulta el estado
      Entonces el estado es "en_entrega"

  Escenario: CEP3 - cambio de estado de en_entrega a entregada
      Dado que el cliente pidio un "menu_familiar"
      Cuando el estado cambia a "entregado"
      Y consulta el estado
      Entonces el estado es "entregado"

  Escenario: CEP4 - cambio de estado de en_preparacion a en_entrega cuando no hay repartidores disponibles
      Dado que el cliente pidio un "menu_familiar"
      Y el estado cambia a "en_entrega"
      Y consulta el estado
      Y el estado es "en_entrega"
      Y que otro cliente pidio un 'menu_familiar'
      Y no hay repartidor disponible
      Cuando el estado cambia a "en_entrega"
      Y consulta el estado
      Entonces el estado es "en_espera"
      
  # Escenario: CANP1 - cancelar pedido recibido
  # Escenario: CANP2 - cancelar pedido en_preparacion
  # Escenario: CANP3 - cancelar pedido en_entrega
  # Escenario: CANP4 - cancelar pedido entregado

  # Escenario: CH1 - Consulta historica sin pedido en curso
  # Escenario: CH2 - Consulta historica con pedido en curso

  # Escenario: CT1 - Consulta con tiempo de entrega estimado menu_individual y lluvia
  # Escenario: CT2 - Consulta con tiempo de entrega estimado menu_individual y sin lluvia
  # Escenario: CT3 - Consulta con tiempo de entrega estimado menu_familiar y lluvia
  # Escenario: CT4 - Consulta con tiempo de entrega estimado menu_pareja y lluvia
  
